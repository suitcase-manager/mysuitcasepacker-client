function clearAuthentication() {
    localStorage.removeItem(VALI_IT_AUTH_TOKEN);
    localStorage.removeItem(VALI_IT_AUTH_USERNAME);
}

function storeAuthentication(session) {
    localStorage.setItem(VALI_IT_AUTH_TOKEN, session.token);
    localStorage.setItem(VALI_IT_AUTH_USERNAME, session.username);
}

function getUsername() {
    return localStorage.getItem(VALI_IT_AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(VALI_IT_AUTH_TOKEN);
}
