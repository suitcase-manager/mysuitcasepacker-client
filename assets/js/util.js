
// General helper functions here...
function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

function getCredentialsFromRegisterContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

function validateRegisterCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}