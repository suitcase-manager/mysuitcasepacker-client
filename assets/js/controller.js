
let allCategories = [];
let allItems = [];
let secondRangeCategory = [];
let filteredCategories = [];
let allGenratedTraveliInfoListsWithItems = [];


function handleNewListButton() {
  document.getElementById("newList").style.display = "block";
  document.getElementById("secondFilterParameter").style.display = "none";
  document.getElementById("form").style.display = "none";
  loadCategoriesWithItems();
}

function handleAddCategoryItem() {
  let newCategoryForm = new FormData();
  newCategoryForm.append("categoryName", document.getElementById("categ").value);
  newCategoryForm.append("itemTitle", document.getElementById("categitem").value);
  postNewCategoryItem(newCategoryForm).then(
    function () {
      loadCategoriesWithItems();

    }
  );
}

function hello() {
  // Pärime kõik need checkboxid, mis on checkitud.
  let allCheckecdCheckboxes = document.querySelectorAll("input:checked, itemcheckbox");
  let packingListName = document.getElementById("packingListName").value;
  let destinationCity = document.getElementById("destinationCity").value;
  let travelDateFrom = document.getElementById("travelDateFrom").value;
  let travelDateTo = document.getElementById("travelDateTo").value;

  // Siia paneme kõik need itemid, mis on selekteeritud.
  let allSelectedItemIds = [];
  for (let i = 0; i < allCheckecdCheckboxes.length; i++) {
    let checkboxId = allCheckecdCheckboxes[i].id;
    checkboxId = checkboxId.split("-")[1];
    allSelectedItemIds.push(checkboxId);
  }

  let tripInfoObj = {
    destinationCity: destinationCity,
    fromDate: travelDateFrom,
    toDate: travelDateTo,
    packingListName: packingListName,
    selectedItems: allSelectedItemIds
  };

  postTripInformation(tripInfoObj).then(
    function () {
      displayTripInformationWithItems();
    }
  );
}

function displayTripInformationWithItems() {
  fetchTripInformationWithItems().then(
    function (tripInfoObj) {
      allGenratedTraveliInfoListsWithItems = tripInfoObj;
      generateDropdownList();
    });
}

function handleDeleteTripInformationWithItems() {
  if (confirm("Are you sure that you want to delete this trip?")) {

    let currentlySelectedTripId = document.getElementById("tripListSelectBox").value;
    deleteTripInformationWithItems(currentlySelectedTripId).then(
      function () {
        document.getElementById("newList").style.display = "none";
        document.getElementById("secondFilterParameter").style.display = "none";
        document.getElementById("form").style.display = "none";
        loadCategoriesWithItems();
        displayTripInformationWithItems();
      });
  }
}

function generateDropdownList() {
  var sel = document.getElementById("tripListSelectBox");
  sel.length = 0;
  var opt = null;
  sel.options = [];
  sel.options[0] = new Option("Choose one of already made lists", "");
  for (i = 0; i < allGenratedTraveliInfoListsWithItems.length; i++) {
    let optionName = allGenratedTraveliInfoListsWithItems[i].packingListName;
    let optionDate = allGenratedTraveliInfoListsWithItems[i].fromDate + " - " + allGenratedTraveliInfoListsWithItems[i].toDate;
    let optionDestination = allGenratedTraveliInfoListsWithItems[i].destinationCity;
    let optionTrip = optionName + ": " + optionDestination + " " + optionDate;
    sel.options[sel.options.length] = new Option(optionTrip, allGenratedTraveliInfoListsWithItems[i].id);
  }

}

function selectTrip() {
  displayCategories(allCategories);
  document.getElementById("newList").style.display = "block";
  document.getElementById("secondFilterParameter").style.display = "block";
  showTravelForm();

  var dd = document.getElementById("tripListSelectBox");
  if (dd.selectedIndex > 0) {
    let currentlySelectedTrip = getSelectedTripInfo(dd.options[dd.selectedIndex].value);
    console.log(currentlySelectedTrip);
    document.getElementById("destinationCity").value = currentlySelectedTrip.destinationCity;
    document.getElementById("travelDateFrom").value = currentlySelectedTrip.fromDate;
    document.getElementById("travelDateTo").value = currentlySelectedTrip.toDate;
    document.getElementById("packingListName").value = currentlySelectedTrip.packingListName;

    let allTripCheckboxes = document.querySelectorAll("input:checked, itemcheckbox");
    for (let i = 0; i < allTripCheckboxes.length; i++) {
      allTripCheckboxes[i].checked = false;
    }

    for (let i = 0; i < currentlySelectedTrip.selectedItems.length; i++) {
      let selectedItemId = "itemcheck-" + currentlySelectedTrip.selectedItems[i];
      console.log(selectedItemId);
      document.getElementById(selectedItemId).checked = true;

    }
  } else {
    document.getElementById("newList").style.display = "none";
    document.getElementById("secondFilterParameter").style.display = "none";
    document.getElementById("form").style.display = "none";
    loadCategoriesWithItems();
    displayTripInformationWithItems();
  }
}

function getSelectedTripInfo(id) {
  for (let i = 0; i < allGenratedTraveliInfoListsWithItems.length; i++) {
    if (allGenratedTraveliInfoListsWithItems[i].id == id) {
      return allGenratedTraveliInfoListsWithItems[i];
    }
  }
  return null;
}

function secondFilterCategoriesWithItemsByTravelParameter(travelParameter) {
  let secondFilteredCategory = [];
  for (let i = 0; i < filteredCategories.length; i++) {
    secondFilteredCategory.push(generateCategorySubTree(filteredCategories[i], travelParameter));
  }
  displayCategories(secondFilteredCategory);
  showTravelForm();
}

function filterCategoriesWithItemsByTravelParameter(travelParameter) {
  filteredCategories = [];
  for (let i = 0; i < allCategories.length; i++) {
    filteredCategories.push(generateCategorySubTree(allCategories[i], travelParameter));

  }
  displayCategories(filteredCategories);
  document.getElementById("secondFilterParameter").style.display = "block";
}

function generateCategorySubTree(category, travelParameter) {
  let newCategory = {
    name: category.name,
    itemList: generateItemsList(category, travelParameter)
  };
  return newCategory;
}

function generateItemsList(category, travelParameter) {
  let filteredItems = [];

  for (let i = 0; i < category.itemList.length; i++) {
    if (doesItemHasCorrectProperty(category.itemList[i], travelParameter)) {
      filteredItems.push(category.itemList[i]);
    }
  }
  return filteredItems;
}

function doesItemHasCorrectProperty(item, travelParameter) {
  for (let i = 0; i < item.propertyList.length; i++) {
    if (item.propertyList[i].name == travelParameter) {
      return true;
    }
  }
  return false;
}

function loadCategoriesWithItems() {
  document.getElementById("packingListName").value = null;
  document.getElementById("destinationCity").value = null;
  document.getElementById("travelDateFrom").value = null;
  document.getElementById("travelDateTo").value = null;

  fetchCategoriesWithItems().then(
    function (categories) {
      allCategories = categories;
      displayCategories(allCategories);
    });
}

function displayCategories(categoriesToDisplay) {
  let itemHtml = "";
  for (let i = 0; i < categoriesToDisplay.length; i++) {
    itemHtml = itemHtml + `
      <b>${categoriesToDisplay[i].name}</b><br>
      ${displayItemList(categoriesToDisplay[i])}
    `;
  }
  document.getElementById("itemsList").innerHTML = itemHtml;
}

function showTravelForm() {
  document.getElementById("form").style.display = "block";
}

function displayItemList(category) {
  let itemsHtml = ``;
  for (let i = 0; i < category.itemList.length; i++) {
    itemsHtml = itemsHtml + `
    <input type="checkbox" class="itemcheckbox" id="itemcheck-${category.itemList[i].id}"> ${category.itemList[i].title}<br>
    `;
  }
  return itemsHtml;
}

function loginUser() {
  let credentials = getCredentialsFromLoginContainer();
  if (validateCredentials(credentials)) {
    login(credentials).then(session => {
      storeAuthentication(session);
      generateTopMenu();
      displayTripInformationWithItems();
      document.getElementById("wrongPassword").style.display = "none";
    }
    )
  }
}

function registerUser() {
  let credentials = getCredentialsFromLoginContainer();
  register(credentials).then(response => {
    if (response.errors.length == 0) {
      document.getElementById("alert").style.display = "block";
      loginUser();
    } else {
      document.getElementById("alertError").style.display = "block";
      logoutUser();
    }
  })
}


function logoutUser() {
  clearAuthentication();
  generateTopMenu();
  showLoginContainer();
}