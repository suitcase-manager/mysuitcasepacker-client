function postNewCategoryItem(categoryItem) {
    return fetch(
        `${API_URL}/addnewitem`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}` 
            },
            body: categoryItem
        }
    );
}


function deleteTripInformationWithItems(id) {
    return fetch(
        `${API_URL}/tripinformation?id=${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}` 
            }
        }
    )
}


function fetchTripInformationWithItems() {
    return fetch(
        `${API_URL}/tripinformation`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}` 
            }
        }
    )
        .then(response => response.json());
}

function postTripInformation(tripInfoObj) {
    return fetch(
        `${API_URL}/tripinformation`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}` 
            },
            body: JSON.stringify(tripInfoObj)
        }
    );
}

function fetchCategoriesWithItems() {
    return fetch(
        `${API_URL}/cathegorieswithitems`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
        .then(response => response.json());
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}
function register(credentials) {
    return fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}


function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        generateTopMenu();
        document.getElementById("wrongPassword").style.display = "block";
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}











































function postSomething(something) {
    return fetch(
        `${API_URL}/something`,
        {
            method: 'POST',
            body: JSON.stringify(something)
        }
    );
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
